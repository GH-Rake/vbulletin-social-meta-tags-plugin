<?
global $vbulletin, $threadinfo, $bloginfo, $pagetitle;

$og_array = [
    'og:description'=>	$vbulletin->options['description'],
	'og:site_name'	=>	$vbulletin->options['bbtitle'],
	'og:title'		=>	$pagetitle,
	'og:type'		=>	'website'
	];

$tc_array = [
    'twitter:card'  => 'summary_large_image'
    ];


if (isset($vbulletin->options['gh_socialmeta_fbapp_id']))
{
    $og_array['fb:app_id'] = $vbulletin->options['gh_socialmeta_fbapp_id'];
}

if (isset($vbulletin->options['gh_socialmeta_fbadmin']))
{
    $og_array['fb:admins'] = $vbulletin->options['gh_socialmeta_fbadmin'];
}

if (isset($vbulletin->options['gh_socialmeta_twitter']))
{
    $tc_array['twitter:site'] = $vbulletin->options['gh_socialmeta_twitter'];
}

//if vbadvanced portal, use default image and description but grab vbAdvanced title
if (THIS_SCRIPT == 'adv_index')
{
	$og_array['og:title']          =   $pagetitle;
	$og_array['og:description']    =   $vbulletin->options['description'];
    $og_array['og:image']          =   $vbulletin->options['gh_socialmeta_image'];
    $og_array['og:url']            =   $vbulletin->options['bburl'];
    
}	

//if not the vbAdvanced portal
else if (THIS_SCRIPT == 'showthread' || THIS_SCRIPT == 'showpost')
{
	$og_array['og:url'] = create_full_url(fetch_seo_url('thread|js|nosession', $threadinfo, null, null, null, true));
	$og_array['og:title'] = $threadinfo['title'];
    $tc_array['twitter:card'] = 'summary';

	if (isset($threadinfo['description']) && !empty($threadinfo['description']))
	{      
        $string = $threadinfo['description'];
		$og_array['og:description'] = substr($string, 0, 300);

        //if a video
        if (stripos($string, 'youtu'))
        {
            $regex = '/https?:\/\/((.+\.youtube\.com\/watch\?v=)|(youtu\.be\/))([a-zA-Z0-9-_]+)/';
            //$regex = '/((http(s)?:\/\/)?)(www\.)?((youtube\.com\/)|(youtu.be\/))[\S]+/'; //new shit gotta change code tho
            preg_match_all($regex, $string, $matches);
            if (!empty($matches))
            {
                //drive traffic to your URL, not youtube
                foreach ($matches[4] as $row)
                {
                    //for you bastards that might try to exploit this
                    if (strlen($row) == 11)
                    {
                        $h = get_headers('http://img.youtube.com/vi/' . $row . '/maxresdefault.jpg');

                        if (stripos($h[0], '404') == false)
                        {
                            $og_array['og:image'] = 'http://img.youtube.com/vi/' . $row . '/maxresdefault.jpg';
                            break;
                        }

                        else
                        {
                            $og_array['og:image'] = 'http://img.youtube.com/vi/' . $row . '/0.jpg';
                            break;
                        }
                    }
                }
                $tc_array['twitter:card'] = 'summary_large_image';
            }
        }

        //If a hot-linked image
		else if (stripos($string, '[/img]') !== false)
		{
			$regex = '#\[img\]\s*(https?://([^*\r\n]+|[a-z0-9/\\._\- !]+))\[/img\]#iUe';
			preg_match_all($regex, $string, $matches);
			if (!empty($matches))
			{
				$og_array['og:image'] = 'http://' . $matches[2][0];
			}
		}

		else if (stripos($string, '[/attach]') !== false)
		{
			$regex = '#\[attach(?:=(right|left|config))?\](\d+)\[/attach\]#i';
			preg_match_all($regex, $string, $matches);

			if (!empty($matches) && isset($matches[2]) && is_array($matches[2]))
			{
				foreach ($matches[2] as $attach_id)
				{
					$og_array['og:image'] = $vbulletin->options['bburl'] . '/attachment.php?attachmentid=' . $attach_id;
				}
			}
		}

        //otherwise use default image and summary_large_image
		else
		{
			$og_array['og:image'] = $vbulletin->options['gh_socialmeta_image'];
            $tc_array['twitter:card'] = 'summary_large_image';
		}
	}

	else if (THIS_SCRIPT == 'entry')
	{
		$og_array['og:url'] = create_full_url(fetch_seo_url('entry|js|nosession', $bloginfo, null, null, null, true));
		$og_array['og:title'] = $bloginfo['title'];
	}

	else if (THIS_SCRIPT == 'vbcms' && isset($vbulletin->vbcms['content_type']) && $vbulletin->vbcms['content_type'] =='Article')
	{
		$og_array['og:url'] = isset($vbulletin->vbcms['page_url']) ? $vbulletin->vbcms['page_url'] : $og_array['og:url'];
		$og_array['og:title'] = $vbulletin->vbcms['title'];
	}
}

if (THIS_SCRIPT == 'showthread'|| (THIS_SCRIPT == 'showpost') || (THIS_SCRIPT == 'entry') || (THIS_SCRIPT == 'vbcms' && isset($vbulletin->vbcms['content_type']) && $vbulletin->vbcms['content_type'] == 'Article'))
{
	$og_array['og:type'] = 'article';
}

if(!empty($og_array['og:image']))
{
    $data = getimagesize($og_array['og:image']);
    $tc_array['twitter:image:width']     = $data[0];
    $tc_array['twitter:image:height']    = $data[1];
}

$rows = '<html prefix="og: http://ogp.me/ns#">' . "\n";

foreach($og_array AS $key => $value)
{
	$rows .= '<meta property="' . $key . '" content="' . $value . '" />' . "\n";
}

foreach($tc_array AS $key => $value)
{
	$rows .= '<meta name="' . $key . '" content="' . $value . '" />' . "\n";
}

$template_hook['headinclude_bottom_css'] .= $rows;
?>