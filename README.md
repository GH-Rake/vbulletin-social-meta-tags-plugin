# vBulletin Social Meta Tags Plugin

My first PHP script which is a vbulletin Plugin for vbulletin 4.2.3
It can be seen on https://guidedhacking.com (no longer, since we upgraded to Xenforo.

## What does this do?

* Adds Open Graph and Twitter Card meta tags to pages for better sharing
* Includes code linking Facebook Domain and app insights to your page
* vbAdvanced Support (the main reason I started modifying this)
* Picks an image from the content with priority being video, then hot linked image, then attached image.
* Grabs thumbnail from youtube video and drives traffic from your social share to your page instead of youtube
* Grabs highest quality thumbnail available for the particular video
* Defaults to a vboptions defined image if no image is found, no more social shares without images or badly resized images
* Tells twitter the aspect ratio of the image for better resizing

This is an example of the meta tags it dynamically adds to every page:

```
#!html

<meta property="og:description" content="Long tutorial here, gonna upload it in 4 seperate parts!  It is 1.5 hours long and will explain everything you need to know to get started using IDA Pro!
Requirements: Intermediate Knowledge of C++ and x86 Assembly"/>
<meta property="og:site_name" content="GuidedHacking How to Hack Games"/>
<meta property="og:title" content="How to Reverse Engineer with IDA Pro Disassembler"/>
<meta property="og:type" content="article"/>
<meta property="fb:app_id" content="1755543248015018"/>
<meta property="fb:admins" content="100009786466728"/>
<meta property="og:url" content="http://guidedhacking.com/showthread.php?7914-How-to-Reverse-Engineer-with-IDA-Pro-Disassembler"/>
<meta property="og:image" content="http://img.youtube.com/vi/fgMl0Uqiey8/maxresdefault.jpg"/>
<meta name="twitter:card" content="summary_large_image"/>
<meta name="twitter:site" content="@guidedhacking"/>
<meta name="twitter:image:width" content="1280"/>
<meta name="twitter:image:height" content="720"/>
```

## Why?

Easier and sexier social media sharing.

Shares will look like this:

![HC13DT6[1].png](https://bitbucket.org/repo/kaXjkg/images/1663670287-HC13DT6%5B1%5D.png)

Instead of boring:

![6w8BZIp[1].png](https://bitbucket.org/repo/kaXjkg/images/2080502280-6w8BZIp%5B1%5D.png)

## Usage
The .php file is just the script.  The .xml file is the plugin that you import into the vBulletin admin control panel's plugin section.

## TODO
Remove BBCode from description tag
Fix bug that occasionally prints wierd shit to the page (not frequent enough for me to fix right now)

## Development History
Wanted to play with PHP and slammed my head into my keyboard for a few days until this thing worked properly

##Credits
Based on [Nexia's OpenGraph Protocol Inserts](http://www.vbulletin.org/forum/showthread.php?t=281290)